#include <malloc.h>

#include "idx.h"

IDX idx_parse(FILE * f) {
    IDX idx = {0};
    
    unsigned int magic    = (0xFF & fgetc(f)) << 0;  
    magic                |= (0xFF & fgetc(f)) << 8;
        
    idx.type              = (0xFF & fgetc(f));
    idx.dimensions        = (0xFF & fgetc(f));
    idx.dimensions_scales = malloc(sizeof(unsigned int) * idx.dimensions);
    
    for (int i = 0; i < idx.dimensions; i++) {
        idx.dimensions_scales[i]  = (0xFF & fgetc(f)) << 24; 
        idx.dimensions_scales[i] |= (0xFF & fgetc(f)) << 16; 
        idx.dimensions_scales[i] |= (0xFF & fgetc(f)) << 8; 
        idx.dimensions_scales[i] |= (0xFF & fgetc(f)) << 0;
    }
    switch (idx.type) {
        case 0x08:
            idx.type_size = 1;
            break;
        case 0x09:
            idx.type_size = 1;
            break;
        case 0x0B:
            idx.type_size = 2;
            break;
        case 0x0C:
            idx.type_size = 4;
            break;
        case 0x0D:
            idx.type_size = 4;
            break;
        case 0x0E:
            idx.type_size = 8;
            break;
    }
    
    int size = 1; for (int i = 0; i < idx.dimensions; i++) size *= idx.dimensions_scales[i];
    
    idx.payload = malloc(idx.type_size * size);
    for (int i = 0; i < size; i++) {
        ((unsigned char*) idx.payload)[i] = 0xFF & fgetc(f);
    }
    return idx;
}
