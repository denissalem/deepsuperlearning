typedef struct IDX_t {
    unsigned char type;
    unsigned int  type_size;
    unsigned char dimensions;
    unsigned int * dimensions_scales;
    void * payload;
} IDX;

IDX idx_parse(FILE * f);
