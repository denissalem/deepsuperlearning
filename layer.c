/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of DeepSuperLearning.
 *
 * DeepSuperLearning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeepSuperLearning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DeepSuperLearning. If not, see <http://www.gnu.org/licenses/>.
*/

#include <malloc.h>
#include <stdlib.h>

#include "layer.h"
#include "neuron.h"

Layer initialize_layer(unsigned int neurons_count, unsigned int input_per_neurons) {
    Layer layer = {0};
    layer.size = neurons_count;
    layer.neurons = malloc(sizeof(Neuron) * neurons_count);
    if (layer.neurons == NULL) {
        exit(-1);
        #ifdef DEBUG
        printf("DSL: Layer initialization failed to allocate neurons.\n");
        #endif
    }
    for (int i = 0; i < neurons_count; i++) {
        initialize_neuron(&layer.neurons[i], input_per_neurons);
    }
    return layer;
}

void terminate_layer(Layer * layer) {
    for (int i=0; i < layer->size; i++) {
        terminate_neuron(&layer->neurons[i]);
    }
    free(layer->neurons);
}
