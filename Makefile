# Copyright 2022 Denis Salem

# This file is part of DeepSuperLearning.

# DeepSuperLearning is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# DeepSuperLearning is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with DeepSuperLearning. If not, see <http://www.gnu.org/licenses/>.


CC = gcc
LINKING = -lm
SOURCE =  test.c \
					activation.c \
					core.c \
					dataset/idx.c \
					layer.c \
					neuron.c \
					vector.c

CFLAGS =
					
test: $(SOURCE)
		$(CC) $(SOURCE) $(LINKING) $(CFLAGS) -o test 

clean:
		rm test
		rm *.o -fv
		rm dataset/*.o -fv
