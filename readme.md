# DeepSuperLearning

Low level framework for deep learning. Highly experimental and made for self education.

## TODO

- Add unit tests
- Python binding
- Do not reinvent the wheel, use low level existing routine for linear algebra.

## WIP

- Add debug mode

## Reading

- https://www.are.na/denis-salem/apprentissage-profond
