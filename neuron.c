/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of DeepSuperLearning.
 *
 * DeepSuperLearning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeepSuperLearning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DeepSuperLearning. If not, see <http://www.gnu.org/licenses/>.
*/

#include <malloc.h>
#include <stdlib.h>

#include "core.h"
#include "neuron.h"

float aggregate(Neuron * neuron, Vector vector) {
    return neuron->bias + dot_product(neuron->weights, vector);
}

void initialize_neuron(Neuron * neuron, unsigned int weight_count) {
    neuron->bias = get_random_float();
    neuron->weights = initialize_vector(weight_count);
    if (neuron->weights.values == NULL) {
        exit(-1);
        #ifdef DEBUG
        printf("DSL: Neuron initialization failed to allocate weights.\n");
        #endif
    }
    for (int i = 0; i< weight_count; i++) {
        neuron->weights.values[i] = get_random_float();
    }
}

void terminate_neuron(Neuron * neuron) {
    free(neuron->weights.values);
}
