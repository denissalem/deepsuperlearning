/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of DeepSuperLearning.
 *
 * DeepSuperLearning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeepSuperLearning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DeepSuperLearning. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DSL_LAYER_H
#define DSL_LAYER_H

#include "neuron.h"

typedef struct Layer_t {
    Neuron * neurons;
    unsigned int size;
} Layer;

Layer initialize_layer(unsigned int neurons_count, unsigned int input_per_neurons);

void terminate_layer(Layer * layer);

#endif
