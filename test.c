/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of DeepSuperLearning.
 *
 * DeepSuperLearning is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DeepSuperLearning is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DeepSuperLearning. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dataset/idx.h"

#include "layer.h"

int main(int argc, char *** argv) {
    IDX dataset = idx_parse(
        fopen("train-images.idx3-ubyte", "rb")
    );
    
    IDX labels = idx_parse(
        fopen("train-labels-idx1-ubyte", "rb")
    );

    Layer layer_1 = initialize_layer(28*28, 28*28);

    for (int i=0; i<layer_1.size; i++) {
        for (int j=0;j<layer_1.neurons[i].weights.size;j++) {
            printf("%f\n", layer_1.neurons[i].weights.values[j]);
        }
    }

    terminate_layer(&layer_1);

    printf("Test completed...\n");    
    return 0;
}
